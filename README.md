# GoMyBlog

#### 项目地址

```
https://gitea.com/shuijianbao/GoMyBlog.git
```

#### 前端使用VUE，后端使用Golang，NGINX做解决跨域问题

1、git push 强制提交

```undefined
git push -f origin master 
```

#### 代理地址
```
export GOPROXY=https://mirrors.aliyun.com/goproxy/
```

```
git fetch --all
git reset --hard origin/master
```

