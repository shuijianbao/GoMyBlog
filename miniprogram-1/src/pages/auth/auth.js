Page({
  data: {
    userInfo: {},
    hasUserInfo: false
  },

  onLoad: function () {
    wx.getUserInfo({
      success: res => {
        console.log("-----------")
        wx.navigateTo({
          url: '/pages/aboutme/aboutme',
        })
      }
    })
  },
  getUserInfo: function (e) {
    wx.setStorageSync('info', e.detail.userInfo)
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})