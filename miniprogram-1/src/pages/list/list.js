// src/pages/list/list.js
Page({

  data: {
    info: {}
  },

  onLoad: function (options) {
    const ui = wx.getStorageSync('info')
    this.setData({
      info: ui
    })

  },

  onShow: function () {

  }
})