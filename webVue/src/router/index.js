import Vue from 'vue'
import VueRouter from 'vue-router'
//引用路由文件总成routes
import routes from "./routes";
Vue.use(VueRouter)

//设置并使用路由模式
const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})
//路由拦截
// router.beforeEach((to, from, next) => {
//     // 获取 JWT Token
//     //l
//     if (localStorage.getItem('JWToken')) {
//         next()
//     } else {
//         if (to.path === '/diary') {
//             next()
//         } else {
//             next('diary')
//         }
//
//     }
//
// });


export default router
