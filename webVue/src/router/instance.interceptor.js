// eslint-disable-next-line no-undef
const instance = axios.create({
    baseURL:  'http://www.laravel5.5.com/api/',
    timeout: 10000,
});

//POST传参序列化(添加请求拦截器)
// 在发送请求之前做某件事
instance.interceptors.request.use(config => {
//     // 设置以 form 表单的形式提交参数，如果以 JSON 的形式提交表单，可忽略
    if(config.method  === 'post'){
        // JSON 转换为 FormData
        const formData = new FormData();
        Object.keys(config.data).forEach(key => formData.append(key, config.data[key]))
        config.data = formData
    }
    // 下面会说在什么时候存储 token
    if (localStorage.token) {
        config.headers['Authorization'] = localStorage.token;
        config.headers['Accept'] = 'application/json';
        // config.headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
        // store.dispatch('logined', localStorage.token)
    }
    return config
},error =>{
    alert("错误的传参", 'fail')
    return Promise.reject(error)
})

// 自定义的 axios 响应拦截器
instance.interceptors.response.use((response) => {
    // 判断一下响应中是否有 token，如果有就直接使用此 token 替换掉本地的 token。你可以根据你的业务需求自己编写更新 token 的逻辑
    var token = response.headers.authorization;

    if (token) {
        // 如果 header 中存在 token，那么触发 refreshToken 方法，替换本地的 token
        axios.defaults.headers.common['Authorization'] = token;
    }
    return response
}, (error) => {
    if (error.response) {
        switch (error.response.status) {
            case 401:
                // 这里写清除token的代码
                router.replace({
                    path: 'login',
                    query: {redirect: router.currentRoute.fullPath}//登录成功后跳入浏览的当前页面
                })
        }
    }
    return Promise.reject(error)
});

Vue.http = Vue.prototype.$http = instance;

//简单的分享一下自己的代码；这是本人结合JWT-Auth定制的axios拦截，
// 1.根据用户是否登陆，查看用户请求头是否携带token
// 2.根据判断后台响应值headers是否携带Authorization值，如果存在，刷新Token
// 3.如果用户授权失败，直接跳至登陆页面
