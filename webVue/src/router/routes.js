import allorder from "../todo/allorder";

const routes = [
    {
        path: '/todo', component: () => import('../todo/Todo'),
        children: [
            {path:'allorder',component:allorder}
        ]
    },
]


export default routes


// {
//     path: '/index',
//         component: () => import('../godear/indexMain/Main'),
//     children: [
//     {
//         path: 'employee',
//         component: () => import('../godear/indexMain/Employee')
//     },
//     {
//         path: 'employeelist',
//         component: () => import('../godear/indexMain/EmployeeList')
//     }
//
// ]
//
// },
