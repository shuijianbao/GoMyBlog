package control

import (
	"github.com/gin-gonic/gin"
	"godear/dao"
	"log"
	"net/http"
)

//记录数据
func RecordContent(c *gin.Context) {
	task := dao.Task{}
	c.BindJSON(&task)

	err := dao.DB.Create(&task).Error
	LoggerDB(err)

	c.JSON(http.StatusOK, gin.H{
		"type":  "success",
		"state": true,
		"data":  task,
	})

}

//结构task查询的人员数据
type TaskList struct {
	ID     int
	State  bool
	Conten string
}

//接收task的方法

func RecordList(c *gin.Context) {
	task := []dao.Task{}
	dao.DB.Find(&task)

	tlists:=[]TaskList{}
	for _, t := range task {
		tlist :=TaskList{
			t.ID,
			t.State,
			t.Conten,
		}
		tlists= append(tlists,tlist)
	}
	c.JSON(http.StatusOK, tlists)

}

func RecordDelete(c *gin.Context) {
	id, ok := c.Params.Get("id")
	if !ok {
		c.JSON(http.StatusOK, gin.H{"error": "无效ID"})
		return
	}
	task := dao.Task{}
	if err := dao.DB.Where("id=?", id).Delete(&task).Error; err != nil {
		c.JSON(http.StatusOK, gin.H{"error": err.Error()})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"type": "success",
		})
	}

}

func RecordPutById(c *gin.Context) {
	id, ok := c.Params.Get("id")
	if !ok {
		c.JSON(http.StatusOK, gin.H{"error": "ID无效"})
		return
	}
	task := dao.Task{}
	if err := dao.DB.Where("id=?", id).First(&task).Error;
		err != nil {
		log.Println(err)
		c.JSON(http.StatusOK, gin.H{"error": err.Error()})
		return
	}
	c.BindJSON(&task)
	err := dao.DB.Save(&task).Error
	if err != nil {
		log.Println(err)
		c.JSON(http.StatusOK, gin.H{"error": err.Error})
		return
	} else {
		c.JSON(http.StatusOK, task)
	}

}
func RecordById(c *gin.Context) {
	id, ok := c.Params.Get("id")
	if !ok {
		log.Println("无效ID")
		c.JSON(http.StatusOK, gin.H{"error": "ID无效"})
	}
	task := dao.Task{}
	err := dao.DB.Where("id = ?", id).First(&task).Error
	if err != nil {
		log.Println(err)
		c.JSON(http.StatusOK, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, task)

}
