package control

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func GoLogin(c *gin.Context) {
	c.JSON(http.StatusOK, "OK")
}

func TestApi(c *gin.Context) {
	var rname Rnames
	err := c.BindJSON(&rname)
	if err!= nil{
		fmt.Println(err)
	}
	c.JSON(http.StatusOK,rname)


}


type Rnames struct {
	Username string `json:"username"`
	Mail string `json:"mail"`
}