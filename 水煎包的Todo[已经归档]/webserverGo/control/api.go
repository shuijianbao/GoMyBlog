package control

import (
	"github.com/gin-gonic/gin"
	"godear/token"
)

func Api() *gin.Engine {
	router := gin.Default()
	//上传员工表

	router.POST("api/login", token.Login)
	router.Use(Cors())

	//v1请求组
	v1 := router.Group("api/v1")
	//进行验证数据的接口
	v1.Use(token.MiddlewareJwt())
	{
		v1.GET("v1", GoLogin)
	}

	v2 := router.Group("api/v2")
	{
		//新增数据并返回id
		v2.POST("recordcontent",RecordContent)
		//获取全部数据
		v2.GET("recordlist",RecordList)
		//删除相关数据
		v2.DELETE("recordelete/:id",RecordDelete)
		//通过id查询并更改数据
		v2.PUT("recordput/:id",RecordPutById)
		//通过id查询相关数据
		v2.GET("recordbyid/:id",RecordById)


	}

	return router
}
