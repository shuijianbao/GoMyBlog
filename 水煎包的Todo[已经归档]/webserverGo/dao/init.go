package dao

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"log"
)

var DB *gorm.DB

func Init() {

	db, err := gorm.Open("sqlite3", "./Dodear.db")
	if err != nil {
		log.Println(err)
	}

	db.AutoMigrate(&User{}, &Task{})

	DB = db
}


