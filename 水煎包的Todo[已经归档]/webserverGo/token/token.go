package token

import (
	"errors"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)
var (
	Secret     = "shuijianbao" // 加盐
	ExpireTime = 7200            // token有效期
)
const (
	ErrorReason_ServerBusy = "服务繁忙"
	ErrorReason_ReLogin    = "请重新登陆"
)

type JWTClaims struct { // token里面添加用户信息，验证token后可能会用到用户信息
	jwt.StandardClaims
	Username string `json:"username"`
}
func Login(c *gin.Context) {
	username := c.PostForm("form")
	if username != "wangjingxue" {
		c.String(http.StatusUnauthorized, "密码错误！")
		return
	}
	claims := &JWTClaims{
		Username: username,
	}
	claims.IssuedAt = time.Now().Unix()
	claims.ExpiresAt = time.Now().Add(time.Second * time.Duration(ExpireTime)).Unix()
	signedToken, err := GetToken(claims)
	if err != nil {
		c.String(http.StatusUnauthorized, err.Error())
		return
	}
	c.String(200, signedToken) //返回相关的JWToken
}
//获取相关的getoken
func GetToken(claims *JWTClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	signedToken, err := token.SignedString([]byte(Secret))
	if err != nil {
		return "", errors.New(ErrorReason_ServerBusy)
	}
	return signedToken, nil
}
//进行相关的JWT进行验证
func verifyAction(strToken string) (*JWTClaims, error) {
	token, err := jwt.ParseWithClaims(strToken, &JWTClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(Secret), nil
	})
	if err != nil {
		return nil, errors.New(ErrorReason_ServerBusy)
	}
	claims, ok := token.Claims.(*JWTClaims)
	if !ok {
		return nil, errors.New(ErrorReason_ReLogin)
	}
	if err := token.Claims.Valid(); err != nil {
		return nil, errors.New(ErrorReason_ReLogin)
	}
	return claims, nil
}


func MiddlewareJwt() gin.HandlerFunc {
	return func(c *gin.Context) {
		getokenJWT := c.Request.Header.Get("authorization")
		claims, err := verifyAction(getokenJWT)
		if err != nil {
			c.String(http.StatusNotFound, err.Error())
			return
		}
		claims.ExpiresAt = time.Now().Unix() + (claims.ExpiresAt - claims.IssuedAt)
		getokenJWT, err = GetToken(claims)
		if err != nil {
			c.String(http.StatusNotFound, err.Error())
			return
		}
		c.Header("authorization", getokenJWT)
		c.Next()
	}
}