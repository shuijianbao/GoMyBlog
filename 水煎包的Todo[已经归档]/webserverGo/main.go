package main

import (
	"godear/dao"
	"godear/control"
)

func main() {
	//连接数据库操作
	dao.Init()
	//这个问题有点调皮
   defer dao.DB.Close()
	r := control.Api()
	r.Run(":9090")
}
//一个待办事宜的小程序