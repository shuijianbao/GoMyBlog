const routes = [
    {
        path: '/main',
        children: [
            {path: 'about', component: () => import('../views/About.vue')}
        ]
    },
    {
        path: '/index',
        component: () => import('../godear/indexMain/Main'),
        children: [
            {
                path: 'employee',
                component: () => import('../godear/indexMain/Employee')
            },
            {
                path: 'employeelist',
                component: () => import('../godear/indexMain/EmployeeList')
            }

        ]

    },
    {
        path: '/login',
        component: () => import('../godear/indexMain/Login')
    },
    {
        path: '/diary',
        component: () => import('../components/diary/DiaryMain')

    },
    {path: '/todo', component: () => import('../todo/Todo')}


]


export default routes