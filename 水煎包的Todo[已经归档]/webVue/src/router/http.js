//后端axios请求地址
import axios from "axios";
//import router from "./index";

let http = axios.create({
    baseURL: 'http://127.0.0.1:9090/api/',
    //headers: {'content-type': 'application/x-www-form-urlencoded'}
    headers: {
        'Content-Type': 'application/json-patch+json; charset=UTF-8'
    }
})
//application/json  application/x-www-form-urlencoded

// "Content-Type": "application/json-patch+json"

//http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

// http.interceptors.request.use(config => {
//
//         if (localStorage.getItem("JWToken")) {
//             config.headers.authorization = localStorage.getItem("JWToken")
//         }
//         return config;
//     },
//     (err) => {
//         return Promise.reject(err)
//     });
//
// http.interceptors.response.use(
//     (res) => {
//         const code = res.status;
//         if (code == 200) {
//             localStorage.setItem("JWToken", res.headers.authorization) //进行保存正确的秘钥
//             return res;
//         }
//         return res;
//     }, (err) => { // 对响应错误做处理
//
//         window.console.log(err)
//         router.push('./diary')
//         localStorage.clear();
//         return Promise.reject(err);
//     }
// )

export default http
