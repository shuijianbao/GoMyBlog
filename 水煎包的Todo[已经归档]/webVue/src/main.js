import Vue from 'vue'
import App from './App.vue'
import './plugins/element.js'

import router from './router'
import http from './router/http'
Vue.config.productionTip = false
//引入tokenstore
import store from "./store/tokenStore";
//使用axios请求路由库
Vue.prototype.$axios = http



new Vue({
    store,
    router,
    render: h => h(App)
}).$mount('#app')
