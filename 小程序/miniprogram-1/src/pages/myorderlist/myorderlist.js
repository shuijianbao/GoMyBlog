// src/pages/myorderlist/myorderlist.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userdata: {},
    goodsname: '',
    remark: '',
    number: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this
    console.log(options.id)
    this.setData({
      id: options.id
    })
    wx.cloud.database().collection('allorder').doc(options.id).get().then(res => {
      console.log(res.data)
      that.setData({
        userdata: res.data,
        goodsname: res.data.goodsname,
        remark: res.data.remark,
        number: res.data.number
      })
    })
  },
  deleteid: function (options) {

    const id = options.currentTarget.dataset.id
    wx.showActionSheet({
      itemList: ['确认删除'],
      success: res => {
        if (res.tapIndex != 1) {
          //通过ID进行删除
          wx.cloud.callFunction({
            name: 'deleteorderbyid',
            data: {
              id: id,
              state: 0
            },
            success: res => {
              wx.switchTab({
                url: '/pages/me/me',
              })
            }
          })

        }
      }
    })

  },
  updata: function (options) {
    const that = this
    const id = options.currentTarget.dataset.id
    wx.cloud.callFunction({
      name: 'deleteorderbyid',
      data: {
        id: id,
        state: 1,
        goodsname: that.data.goodsname,
        remark: that.data.remark,
        number: that.data.number
      },
      success: res => {
        console.log(res)
        wx.switchTab({
          url: '/pages/me/me',
        })
      }
    })

  },
  bindKeyInput1: function (e) {
    this.setData({
      goodsname: e.detail.value,
    })
  },
  bindKeyInput2: function (e) {
    this.setData({
      remark: e.detail.value,
    })
  },
  bindKeyInput3: function (e) {
    this.setData({
      number: e.detail.value,
    })
  },
})