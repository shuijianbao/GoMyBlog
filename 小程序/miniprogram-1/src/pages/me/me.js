//这个逻辑应该是，首先进入me后清除storge中的userinfo然后获取最新的数据展示用户个人信息的界面，同时刷新个人信息，并支持修改个人信息
Page({
  data: {
    userinfo: {},
    openid: '',
    state: false,
    checklogin: '',
    wdfb: '',
    wdjs: '',
    hbph: '',
    ddlq: ''
  },
  //首先判断用户是否已经注册，如果注册了就展示，没有注册的话就跳转到注册界面
  onLoad: function () {

    this.onmeshow()


  },
  onShow: function () {
    this.updatabase()
    this.checklogined()
    this.getmeorder()

  },
  //跳轉修改界面
  makeamend: function (e) {
    const id = e.currentTarget.dataset.id
    const url = '/pages/myorderlist/myorderlist?id=' + id
    wx.navigateTo({
      url: url,
    })


  },
  //獲取用戶的訂單
  getmeorder: function () {
    let that = this
    wx.cloud.callFunction({
      name: 'getmeorder',
      data: {
        state: 0
      },
      success: res => {
        console.log(res)
        that.setData({
          myorderlist: res.result.list.data
        })
      }
    })
  },
  onmeshow: function () {
    const that = this
    wx.cloud.callFunction({
      name: 'onMeShow',
      success: res => {
        if (res.result.getchecklogin.total == 1) {
          wx.setStorageSync('openid', res.result.openid)

          this.setData({
            state: true
          })
          setTimeout(res => {
            that.setData({
              state: false
            })
          }, 2500)
        } else {
          wx.setStorageSync('loginstate', 0)
          wx.navigateTo({
            url: '/pages/input/input',
          })
        }
        that.setData({
          checklogin: res.result.getchecklogin.total,
          wdfb: res.result.getwdfb.total,
          wdjs: res.result.getwdjs.total,
          hbph: res.result.gethbph.total,
          ddlq: res.result.getddlq.total,
          openid: res.result.openid
        })
      },
      fail: res => {
        console.log(res)
      }
    })
  },
  checklogined: function (e) {
    let that = this
    wx.clearStorageSync('userinfo') //先清除再获取
    wx.getUserInfo({
      fail: res => {
        console.log("没有授权！请点击授权，跳转到授权页面")
        wx.navigateTo({
          url: '/pages/auth/auth',
        })
      },
      success: (res) => {
        console.log("成功授权")
        that.setData({
          userinfo: res.userInfo
        })
        wx.setStorageSync('userinfo', res.userInfo)

      },
    })
  },
  updatabase: function () {
    let that = this
    wx.cloud.callFunction({
      name: 'onMeShow',
      success: res => {

        that.setData({
          checklogin: res.result.getchecklogin.total,
          wdfb: res.result.getwdfb.total,
          wdjs: res.result.getwdjs.total,
          hbph: res.result.gethbph.total,
          ddlq: res.result.getddlq.total,
          openid: res.result.openid
        })
        wx.setStorageSync('openid', res.result.openid)
        if (res.result.getchecklogin.total == 0) {
          wx.navigateTo({
            url: '/pages/input/input',
          })
        }
      }
    })


  }




})