// src/pages/auth/auth.js
Page({

  data: {
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  bingGetUserInfo: function (e) {
    if (e.detail.userInfo) {
      console.log("获取用户信息授权成功")
      wx.switchTab({
        url: '/pages/me/me',
      })
    } else {
      console.log("获取用户信息授权失败")
      wx.showActionSheet({
        itemList: ['请点击获取授权', '确定'],
        success: res => {
          console.log(res.tapIndex)
        }
      })

    }
  }
})