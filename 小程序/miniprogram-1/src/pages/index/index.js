Page({
  data: {
    timestampt: '',
    localtime: '',
    orderid: '',
    hrmusers: {},
    userinfo: {},
    message: false,
    hidden: true,
    focus: false,
    inputValue1: '',
    inputValue2: '',
    inputValue3: '1',
  },
  onShow: function () {
    this.timeandorder()
  },
  //获取订单和时间戳
  timeandorder: function () {
    this.finduserinfo()
    let id = this.genID(3)
    let s = new Date().getTime()
    let timesta = this.getlocaltime(s)
    this.setData({
      orderid: id,
      localtime: timesta,
      timestampt: s
    })
  },
  // 获取当前用户的信息
  finduserinfo: function () {
    let that = this
    let ui = wx.getStorageSync('userinfo')
    this.setData({
      userinfo: ui
    })
    wx.cloud.database().collection('hrmusers').where({
      openid: that.data.userinfo.openid
    }).limit(1).get().then(res => {
      that.setData({
        hrmusers: res.data[0]
      })
    })
  },
  //生成订单ID
  genID(length) {
    return Number(Math.random().toString().substr(3, length) + Date.now()).toString(36);
  },
  //将时间戳转换成时间格式
  getlocaltime: function (ns) {
    return new Date(parseInt(ns)).toLocaleString().replace(/年-|月/g, "-").replace(/日/g, " ");
  },

  bindKeyInput1: function (e) {
    this.setData({
      inputValue1: e.detail.value,
    })
  },
  bindKeyInput2: function (e) {
    this.setData({
      inputValue2: e.detail.value,
    })
  },
  bindKeyInput3: function (e) {
    if (!isNaN(e.detail.value)) {
      if (e.detail.value > 10) {
        e.detail.value = 10
      } else if (e.detail.value < 1) {
        e.detail.value = 1
      }
      this.setData({
        inputValue3: e.detail.value,
      })
    }
  },
  //发布按钮
  allclick: function () {
    var that = this
    this.setData({
        hidden: false
      }),
      wx.showActionSheet({
        itemList: ["确认"],
        success(res) {
          if (res.tapIndex == 0) {
            console.log("确认") //上传订单
            const ui = wx.getStorageSync('userinfo')
            const openid = wx.getStorageSync('openid')
            wx.cloud.callFunction({
              name: 'createorder',
              data: {
                phone: that.data.hrmusers.phone,
                taobaoname: that.data.hrmusers.taobaoname,
                publishopenid: openid, //发布人ID
                recipientopenid: 0, //领用人openID

                createdata: that.data.localtime, //创建日期
                recipientdata: 0, //领用人接单时间

                orderid: that.data.orderid, //订单号
                goodsname: that.data.inputValue1, //物品名称
                number: that.data.inputValue3, //数量
                location: that.data.hrmusers.adress, //领取地点

                iconsrc: ui.avatarUrl, //头像连接地址
                remark: that.data.inputValue2, //备注信息
                state: 0, //订单状态 0 未领取 1 已接单 2 已送达 3 
              },
              success: res => {
                that.setData({
                  message: true
                })
                setTimeout(() => {
                  that.setData({
                    message: false
                  })
                  wx.switchTab({
                    url: '/pages/list/list',
                  })
                }, 3000)
                that.setData({
                  inputValue1: '',
                  inputValue2: '',
                  inputValue3: '1',
                })

              }
            })



          }
        },
      })
  }
})