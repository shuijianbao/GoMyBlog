Page({
  data: {
    list: [],
    timestampt: '',
    localtime: '',
    orderid: '',
    hrmusers: {},
    userinfo: {},
    message: false,
    hidden: true,
    focus: false,
    inputValue1: '',
    inputValue2: '',
    inputValue3: '1',
  },
  onShow: function () {
    this.getallorderlist()

  },
  getallorderlist: function () {
    const that = this
    wx.cloud.callFunction({
      name: 'getallorderlist',
      data: {
        state: 0
      },
      success: res => {
        console.log("获取数据成功")
        that.setData({
          list: res.result.database.data
        })
      },
      fail: res => {
        console.log(res)
      }
    })
  },
  reception: function (e) {
    const id = e.currentTarget.dataset.id
    const that = this
    wx.cloud.callFunction({
      name: 'deleteorderbyid',
      data: {
        id: id,
        idstate: 1,
      },
      success: res => {
        console.log(res.result)
        this.getallorderlist()
      }
    })
  }

})