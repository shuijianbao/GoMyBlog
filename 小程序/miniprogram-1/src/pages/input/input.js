// 这个暂时明白了，目的就是单纯的注册授权并注册，me界面是为了跳转的

Page({
  data: {
    taobaoname: '',
    phone: '',
    adress: '',
    userInfo: {},
    openid: '',
    checklogin: '',
    authSetting: true,
    hadusers: false
  },
  onShow: function () {

    this.checkopenid()

  },
  // 获取用户的个人信息
  onGotUserInfo: function (e) {
    let that = this
    wx.cloud.callFunction({
      name: 'login',
      success: res => {
        if (e.detail.userInfo) {
          //用户同意i授权
          console.log(e.detail.userInfo)
          that.setData({
            authSetting: false,
            openid: res.result.openid,
            userInfo: e.detail.userInfo
          })
          e.detail.userInfo.openid = res.result.openid

          wx.setStorageSync('userinfo', e.detail.userInfo)
          // 注册
          wx.showActionSheet({
            itemList: ['确认注册'],
            success(res) {

              if (res.tapIndex == 0) {
                that.submithrm()
                wx.switchTab({
                  url: '/pages/me/me',
                })
              }
            },
            fail(res) {
              console.log("用户取消:授权：", res.errMsg)
            }
          })
        } else {
          console.log("e.detail.userInfo")
          console.log(res.result)
        }
      },
      fail: res => {
        console.log("失败", res)
      }
    })
  },

  // 查询有没有这个openid，如果有的话直接跳转到me
  checkopenid: function (e) {
    let that = this
    wx.cloud.callFunction({
      name: 'onMeShow',
      success: res => {
        that.setData({
          checklogin: res.result.getchecklogin.total,
          openid: res.result.openid
        })
        if (res.result.getchecklogin.total == 1) {
          wx.switchTab({
            url: '/pages/me/me',
          })
        }
      }
    })
  },

  //form的双向绑定
  taobaonamefunc: function (e) {
    this.setData({
      taobaoname: e.detail.value
    })

  },
  phonefunc: function (e) {
    this.setData({
      phone: e.detail.value
    })

  },
  adressfunc: function (e) {
    this.setData({
      adress: e.detail.value
    })

  },
  //提交这三个数据到数据库
  submithrm: function () {
    let that = this
    wx.cloud.callFunction({
      name: 'joinus',
      data: {
        openid: that.data.openid,
        taobaoname: that.data.taobaoname,
        phone: that.data.phone,
        adress: that.data.adress
      },
      success: res => {
        console.log(res)
      }
    })

  }


})


















// openSetting: function () {
//   let that = this
//   wx.openSetting({
//     success: res => {
//       console.log("点击用户授权操作-", res.authSetting['scope.userInfo'])
//       if (res.authSetting['scope.userInfo']) {
//         console.log("成功获取授权-在这里我就可以请求外部接口获取用户的个人信息了实际上")
//         that.setData({
//           authSetting: true
//         })
//       } else {
//         wx.showActionSheet({
//           itemList: ['取消授权将无法获得您的注册信息'],
//           success(res) {

//             console.log('取消授权将无法获得您的注册信息')

//           },
//           fail(res) {
//             console.log("用户取消授权：", res.errMsg)
//             that.setData({
//               authSetting: false
//             })
//           }
//         })


//       }
//     }
//   })
// },