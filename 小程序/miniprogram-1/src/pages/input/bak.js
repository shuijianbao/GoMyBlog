Page({
  data: {
    userinfo: {},
    setokenok: false,
    openid: "",
    message: false,
    taobaoname: "",
    phone: "",
    adress: ""
  },
  // 判定输入为非空字符
  formSubmit: function (e) {
    var taobaoname = e.detail.value.taobaoname;
    var phone = e.detail.value.phone;
    var adress = e.detail.value.adress;
    var getad = this.checkAuthorization()
    if (!getad) {
      wx.showModal({
        content: '必须获取用户授权',
        success: function (res) {
          if (res.confirm) {
            console.log('用户点击确定')
          }
        },
        fail: (res) => {
          console.log("fail")
        }
      })
    } else {
      if (taobaoname == "" || phone == "" || adress == "") {
        wx.showModal({
          content: '请输入完整信息！',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            }
          },
          fail: (res) => {
            console.log("fail")
          }
        })
      } else {
        //成功调用
        this.setData({
          taobaoname: taobaoname,
          phone: phone,
          adress: adress
        })
        this.allclick()



      }
    }
  },
  // 手机号部分
  inputPhoneNum: function (e) {
    let phoneNumber = e.detail.value
    if (phoneNumber.length === 11) {
      let checkedNum = this.checkPhoneNum(phoneNumber)
      // 通过check返回值
      if (!checkedNum) {
        wx.showModal({
          title: '提示',
          content: '手机号码不正确',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            }
          },
          fail: (res) => {
            console.log("fail")
          }
        })
      }
      this.setData({
        phone: ''
      })

    } else {
      console.log("手机号输入错误")
      wx.showModal({
        title: '提示',
        content: '手机号码不正确',
        success: function (res) {
          if (res.confirm) {
            console.log('用户点击确定')
          }
        },
        fail: (res) => {
          console.log("fail")
        }
      })
    }
  },
  checkPhoneNum: function (phoneNumber) {
    let str = /^1\d{10}$/
    if (str.test(phoneNumber)) {
      return true
    } else {
      wx.showToast({
        title: '手机号不正确',
        image: './../../../../images/fail.png'
      })
      return false
    }
  },
  checkAuthorization: function () {
    let ui = wx.getStorageSync('userinfo')
    if (ui.openid) {
      return true
    } else {
      return false
    }
  },
  // 通用确认框
  allclick: function () {
    let ui = wx.getStorageSync('userinfo')
    console.log(ui)
    let that = this
    this.setData({
        userinfo: ui
      }),
      wx.showActionSheet({
        itemList: ["确认"],
        success(res) {
          console.log(res.tapIndex)
          if (res.tapIndex == 0) {
            // 将数据上传到小程序中

            that.uphrmuser()

          }
        },
      })
  },
  //获取用户的个人信息这个主要是openid的授权
  onGotUserInfo: function (e) {
    let that = this
    let ui = wx.getStorageSync('userinfo')
    if (!ui.openid) {
      wx.cloud.callFunction({
        name: "login",
        success: res => {
          that.setData({
            openid: res.result.openid,
            userinfo: e.detail.userInfo,
          })
          // 将个人的文件信息保存到本地文件中做持久化存储
          that.data.userinfo.openid = that.data.openid
          wx.setStorageSync('userinfo', that.data.userinfo)
          console.log(res.result.openid)
          that.setData({
            openid: res.result.openid
          })
        },
        fail: res => {
          wx.navigateBack({
            delta: -1
          })
        }
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '已获得授权无需重复获取',
        success: function (res) {
          if (res.confirm) {
            console.log('用户点击确定')
          }
        },
        fail: (res) => {
          console.log("fail")
        }
      })
    }
  },
  //搞事！上传信息到个人hrmusers
  uphrmuser: function (e) {
    let that = this
    const db = wx.cloud.database()
    db.collection('hrmusers').where({
      openid: that.data.openid
    }).get().then(res => {
      if (res.data.length == 0) {
        wx.cloud.callFunction({
          name: 'joinus',
          data: {
            openid: that.data.openid,
            taobaoname: that.data.taobaoname,
            phone: that.data.phone,
            adress: that.data.adress
          },
          success: res => {
            console.log("ojbk!!!")
          },
          fail: res => {
            console.log("error")
          }
        })
      }else{
        wx.switchTab({
          url: '/pages/me/me',
        })
      }
    })

    







    // 检查是否已经上传过个人信息
  }
})