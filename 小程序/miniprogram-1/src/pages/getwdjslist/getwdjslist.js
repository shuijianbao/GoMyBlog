Page({
  data: {
    list: [],
    timestampt: '',
    localtime: '',
    orderid: '',
    hrmusers: {},
    userinfo: {},
    message: false,
    hidden: true,
    focus: false,
    inputValue1: '',
    inputValue2: '',
    inputValue3: '1',
  },
  onShow: function () {
    this.getallorderlist()

  },
  getallorderlist: function () {
    const that = this
    const ui = wx.getStorageSync('openid')
    wx.cloud.callFunction({
      name: 'getallorderlist',
      data: {
        state: 1,
        receiptionopenid: ui
      },
      success: res => {
        console.log("获取数据成功")
        console.log(res.result.database.data)
        that.setData({
          list: res.result.database.data
        })
      },
      fail: res => {
        console.log(res)
      }
    })
  },
  reception: function (e) {
    const id = e.currentTarget.dataset.id
    const that = this
    wx.cloud.callFunction({
      name: 'deleteorderbyid',
      data: {
        id: id,
        idstate: 2,
      },
      success: res => {
        console.log(res.result)
        this.getallorderlist()
      }
    })


  },


})