// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  console.log("云函数调用中。。。。")
  const sum = await db.collection('allorder').where({
    openid:event.openid
  }).count()
  return sum
}