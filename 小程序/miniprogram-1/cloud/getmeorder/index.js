// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const dbs = cloud.database().collection('allorder')
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const openid = wxContext.OPENID
  const list = await dbs.where({
    publishopenid: openid,
    state: event.state
  }).get()
  const allorderlist = await dbs.where({
    publishopenid: openid
  }).get()
  return {
    list: list,
    allorderlist: allorderlist
  }
}