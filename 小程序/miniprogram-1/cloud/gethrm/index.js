// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  try {
    //获取hrmuser表的数据
    return await db.collection('hrmusers').where({
      openid: event.openid
    }).get()
  } catch (e) {
    console.log(e)
  }
}