// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  try {
    return await db.collection('allorder').add({
      data: {
        taobaoname: event.taobaoname,
        publishopenid: event.publishopenid, //发布人ID
        phone: event.phone, //发布人的手机号码
        recipientopenid: event.recipientopenid, //领用人openID

        createdata: event.createdata, //创建日期
        recipientdata: event.recipientdata, //领用人接单时间

        orderid: event.orderid, //订单号
        goodsname: event.goodsname, //物品名称
        number: event.number, //数量
        location: event.location, //领取地点

        iconsrc: event.iconsrc, //头像连接地址
        remark: event.remark, //备注信息
        state: event.state, //订单状态 0 未领取 1 已接单 2 已送达 3 
      }
    })
  } catch (e) {
    console.log(e)
  }
}