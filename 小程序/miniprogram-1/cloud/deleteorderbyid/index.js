// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const dbs = cloud.database().collection('allorder')
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  if (event.state == 0) {
     await dbs.doc(event.id).remove()
  } else if(event.state==1) {
    await dbs.doc(event.id).update({
      data: {
        goodsname: event.goodsname,
        remark: event.remark,
        number: event.number
      }
    })
  }else{
    //更新接单人员
    await dbs.doc(event.id).update({
      data:{
        state:event.idstate,
        recipientopenid:wxContext.OPENID,
      }
    })
  }

  return "success"
}