// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  console.log("调用login登陆云函数")
  return {
    userinfo: event.userinfo,
    openid: wxContext.OPENID
  }
}