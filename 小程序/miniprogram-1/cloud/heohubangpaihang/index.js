// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const openid = wxContext.OPENID
  // 可以考虑直接云端请求openID不就更好吗

  return await db.collection('allorder').where({
    publishopenid: openid
  }).count()
}