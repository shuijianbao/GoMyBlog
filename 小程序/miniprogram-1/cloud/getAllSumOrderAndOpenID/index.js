// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const dbs = db.collection('allorder')
  const getwdfb = await dbs.where({
    publishopenid: wxContext.OPENID
  }).count()
  const getwdjs = await dbs.where({
    recipientopenid: wxContext.OPENID,
    state: 1
  }).count()
  const gethbph = await dbs.where({
    recipientopenid: wxContext.OPENID
  }).count()
  const getddlq = await dbs.where({
    publishopenid: wxContext.OPENID,
    state: 0
  }).count()

  return {
    event,
    getwdfb: getwdfb,
    getwdjs: getwdjs,
    gethbph: gethbph,
    getddlq: getddlq
  }
}