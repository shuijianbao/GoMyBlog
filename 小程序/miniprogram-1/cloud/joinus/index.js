// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()

const db = cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {
  try {
    return await db.collection('hrmusers').add({
      data: {
        openid: event.openid,
        taobaoname: event.taobaoname,
        phone:event.phone,
        adress:event.adress
      }
    })

  } catch (e) {
    console.log(e)
  }
}