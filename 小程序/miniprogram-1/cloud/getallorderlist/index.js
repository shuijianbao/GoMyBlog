// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const dbs = cloud.database().collection('allorder')
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  var database
  if (event.receiptionopenid) {
    database = await dbs.where({
      state: event.state,
      recipientopenid: wxContext.OPENID,
    }).orderBy('createdata', 'desc').get()
  } else {
    database = await dbs.where({
      state: event.state,
    }).orderBy('createdata', 'desc').get()
  }
  return {
    event,
    openid: wxContext.OPENID,
    appid: wxContext.APPID,
    unionid: wxContext.UNIONID,
    database: database
  }
}