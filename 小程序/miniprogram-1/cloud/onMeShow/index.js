// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const dbs = cloud.database().collection('allorder')
const hrms = cloud.database().collection('hrmusers')

// 云函数入口函数

exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const checklogin = await hrms.where({
    openid: wxContext.OPENID
  }).count()

  const getwdfb = await dbs.where({
    publishopenid: wxContext.OPENID
  }).count()
  const getwdjs = await dbs.where({
    recipientopenid: wxContext.OPENID,
    state: 1
  }).count()
  const gethbph = await dbs.where({
    recipientopenid: wxContext.OPENID
  }).count()
  const getddlq = await dbs.where({
    publishopenid: wxContext.OPENID,
    state: 0
  }).count()
  const getwdjslist = await dbs.where({
    recipientopenid: wxContext.OPENID,
    state: 1
  }).get()

  return {
    event,
    openid: wxContext.OPENID,
    getchecklogin: checklogin,
    getwdfb: getwdfb,
    getwdjs: getwdjs,
    gethbph: gethbph,
    getddlq: getddlq,
    getwdjslist: getwdjslist,

  }
}