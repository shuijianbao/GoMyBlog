// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const getstatewherezero = await db.collection('allorder').where({
    state: 1,
    publishopenid: event.publishopenid
  }).count()

  return getstatewherezero
}