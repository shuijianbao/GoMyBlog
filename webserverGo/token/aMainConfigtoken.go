package token

import (
	jwt "github.com/dgrijalva/jwt-go"
)

var (
	Secret     = "shuijianbao" // 加盐
	ExpireTime = 7200          // token有效期
)

const (
	ErrorReason_ServerBusy = "服务繁忙"
	ErrorReason_ReLogin    = "验证信息不正确"
)
// token里面添加用户信息，验证token后可能会用到用户信息
type JWTClaims struct {
	jwt.StandardClaims
	ID       int
	Phone    string `json:"phone"`
}




