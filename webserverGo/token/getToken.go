package token

import (
	"errors"
	jwt "github.com/dgrijalva/jwt-go"
	"time"
)

//获取相关的claims 并返回token

func GetToken(claims *JWTClaims) (string, error) {

	claims.IssuedAt = time.Now().Unix()
	claims.ExpiresAt = time.Now().Add(time.Second * time.Duration(ExpireTime)).Unix()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	signedToken, err := token.SignedString([]byte(Secret))
	if err != nil {
		return "", errors.New(ErrorReason_ServerBusy)
	}
	return signedToken, nil
}
