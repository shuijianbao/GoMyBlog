package dao

//用户表
type Hrmusers struct {
	ID    int    `json:"id";gorm:"primary_key"`
	Name  string `json:"name"`
	Phone string `json:"phone"`

}
//所有的订单表
type AllOrders struct {
	ID           int    `json:"id"`
	Content      string `json:"content"`
	HrmuserID    int    `json:"hrmuser_id"`
	ResHrmuserID int    `json:"res_hrmuser_id"`
	State        int    `json:"state"`
}
//state 中 0 表示未领取 1 表示已经领取 2 表示已经归还给发布人员