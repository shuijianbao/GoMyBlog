package dao

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"log"
)

var DB *gorm.DB
var Dialect = "sqlite3"
var SqlDir = "./database.db"

func Init() {
	//连接数据库
	db, err := gorm.Open(Dialect, SqlDir)
	if err != nil {
		log.Println(err)
	}
	//创建数据库模型
	db.AutoMigrate(
		&Hrmusers{},
		&AllOrders{},
	)

	//db.LogMode(true)
	DB = db
}
