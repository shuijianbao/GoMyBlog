package main
/*
这个小dome主要是练手的小项目，
使用人可以发布自己的快递信息等待别人领用
同样如果能力允许的话希望后期能够进行打赏功能的实现
*/


import (
	"godear/control"
	"godear/dao"
)

func main() {
	//连接数据库操作
	dao.Init()
	defer dao.DB.Close()
	router := control.Goexpress()
	router.Run(":9090")
}

