package control

import (
	"github.com/gin-gonic/gin"
	"godear/server"
)

func Goexpress() *gin.Engine {

	router := gin.Default()

	group := router.Group("coco")
	{
		group.POST("login",server.Login)
	}
	routerGroup := router.Group("api")
	{
		routerGroup.POST("topexpress",server.ExpressInfor)
		routerGroup.DELETE("order/:id",server.DeleteOrderInfro)
	}
	routerUser:=router.Group("usercontrol")
	{
		routerUser.POST("create",server.CreateHrm)
		routerUser.DELETE("delete/:id",server.DeleteHrmByID)
	}
	//订单的状态变更
	routerOrder:=router.Group("getorder")
	{
		routerOrder.GET("orderlist/:state",server.FindMyStateNotReceive)
		//查看所有的订单信息，这个后期应该想办法进行分页处理哈 暂时不晓得如何分页 先搞一个吧
		routerOrder.GET("orderallist",server.FindAllOrder)
		//接单变更all中的字段 res_hrmuser_id 并变更状态state 这个是通用的接口 主要是前端可以方便变更，也是这个小dome的主要入口之一
		routerOrder.POST("reshrmuserid/:state",server.ResHrmUserID)
		//这个接口主要是为了获取res_hrmuser_id为自己的订单，主要作用是。。查看自己抢到的订单
		routerOrder.POST("reshrmidbyself/:state",server.ResHrmUserIDByself)
	}


	return router
}
