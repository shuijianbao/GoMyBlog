package server

import (
	"godear/dao"
	"godear/token"
)

//通过上传者的token转换成id信息



func GetJwtID(t string) (*dao.UpJwtById, error) {
	action, err := token.VerifyAction(t)
	if err != nil {
		return nil, err
	}
	j := &dao.UpJwtById{
		ID:    action.ID,
		Phone: action.Phone,
	}
	return j, nil
}