package server

import (
	"github.com/gin-gonic/gin"
	"godear/dao"
	"godear/token"
	"net/http"
)

//接收的信息便利到相关的结构体中

func Login(c *gin.Context) {
	var hrm dao.Hrmusers
	//获取登录的信息并进行验证
	var loginstruct dao.UpJwtById
	err1 := c.BindJSON(&loginstruct)
	if err1 != nil {
		c.JSON(http.StatusOK, err1)
		return
	}
	dao.DB.Where("phone=?", loginstruct.Phone).First(&hrm)
	if loginstruct.Name != hrm.Name {
		c.JSON(http.StatusOK, "密码错误")
		return
	}
	claims := &token.JWTClaims{
		ID:    hrm.ID,
		Phone: hrm.Phone,
	}
	//将获取到的
	signedToken, err := token.GetToken(claims)
	if err != nil {
		c.String(http.StatusOK, err.Error())
		return
	}
	//返回相关的JWToken
	c.String(http.StatusOK, signedToken)
}

type dog struct {
	dao.UpJwtById
}

func (t *dog) FindByPhone() error {
	var hrm dao.Hrmusers
	var loginstruct dao.UpJwtById
	err := dao.DB.Where("phone=?", loginstruct.Phone).First(&hrm).Error
	return err
}
