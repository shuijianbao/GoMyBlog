package server

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method

		c.Header("Access-Control-Allow-Origin", "*")         //必选
		c.Header("Access-Control-Allow-Headers", "*")        //可选  如果request有header, 必选
		c.Header("Access-Control-Allow-Credentials", "true") //可选
		c.Header("Access-Control-Allow-Methods", "*")        //可选
		c.Header("Access-Control-Expose-Headers", "*")       //可选

		//放行所有OPTIONS方法
		if method == "OPTIONS" {
			c.AbortWithStatus(http.StatusOK)
		}
		// 处理请求
		c.Next()
	}
}

