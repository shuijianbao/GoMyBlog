package server

import (
	"github.com/gin-gonic/gin"
	"godear/token"
	"net/http"
	"time"
)

//验证JWT中间件
func MiddlewareJwt() gin.HandlerFunc {
	return func(c *gin.Context) {
		//获取authorization
		getokenJWT := c.Request.Header.Get("authorization")
		claims, err := token.VerifyAction(getokenJWT)
		if err != nil {
			c.String(http.StatusOK, err.Error())
			return
		}
		//验证成功颁发新的token
		claims.IssuedAt = time.Now().Unix()
		claims.ExpiresAt = time.Now().Unix() + (claims.ExpiresAt - claims.IssuedAt)
		//调用getoken获取JWT
		getokenJWT, err = token.GetToken(claims)
		if err != nil {
			c.String(http.StatusOK, err.Error())
			return
		}
		//将获取到的JWT返回到数据的 header 中
		c.Header("authorization", getokenJWT)
		c.Next()
	}
}
