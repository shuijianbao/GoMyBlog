package server

import (
	"github.com/gin-gonic/gin"
	"godear/dao"
	"log"
	"net/http"
	"strconv"
	"strings"
)

//接收上传的信息并保存到数据库中

func ExpressInfor(c *gin.Context) {
	//获取人员中的ID
	ex := c.Request.Header.Get("authorization")

	s := strings.Fields(ex)
	ex = s[1]

	getJwtID, err := GetJwtID(ex)
	if err != nil {
		c.JSON(http.StatusOK, err.Error())
		return
	}
	//将上传的topexpress绑定到topexpress
	topexpress := &dao.TopExpress{}
	err = c.BindJSON(topexpress)
	if err != nil {
		log.Println(err)
		c.JSON(http.StatusOK, err.Error())
		return
	}
	allorder := &dao.AllOrders{
		Content:   topexpress.Content,
		HrmuserID: getJwtID.ID,
		State:     0,
	}

	dao.DB.Create(&allorder)

	c.JSON(http.StatusOK, "上传信息成功")
}
func DeleteOrderInfro(c *gin.Context) {
	//通过ID进行定位删除订单
	param := c.Param("id")
	var order dao.AllOrders
	err2 := dao.DB.First(&order, param).Error
	if err2 != nil {
		log.Println(err2)
		c.JSON(http.StatusOK, err2.Error())
		return
	}
	dao.DB.Delete(&order)
	c.JSON(http.StatusOK, "it is ok!")
}
func FindMyStateNotReceive(c *gin.Context) {
	state := c.Param("state")
	ex := c.Request.Header.Get("authorization")
	sex := strings.Fields(ex)
	ex = sex[1]
	hrmID, _ := GetJwtID(ex)
	log.Println(hrmID.ID)

	var order []dao.AllOrders
	log.Println("2")
	find := dao.DB.Where("state=? AND hrmuser_id=?", state, hrmID.ID).Find(&order).Error
	if find != nil {
		log.Println(find.Error)
		return
	}
	c.JSON(http.StatusOK, order)
}

//查看所有的订单信息，这个后期应该想办法进行分页处理哈 暂时不晓得如何分页 先搞一个吧
func FindAllOrder(c *gin.Context) {
	var order []dao.AllOrders
	find := dao.DB.Where("state=?", 0).Find(&order).Error
	if find != nil {
		log.Println(find.Error)
		return
	}
	c.JSON(http.StatusOK, order)
}

//显示获取这个订单的ID，通过这个ID订单查找对应的数据，然后将订单中的ResHrmUserID惊醒状态更改
func ResHrmUserID(c *gin.Context) {
	atoi, e := strconv.Atoi(c.Param("state"))
	if e != nil {
		c.JSON(http.StatusOK, e.Error())
		return
	}
	fields := strings.Fields(c.Request.Header.Get("authorization")) //测试使用暂时这样
	jwtID, _ := GetJwtID(fields[1])
	var order dao.AllOrders
	err := c.BindJSON(&order)
	if err != nil {
		c.JSON(http.StatusOK, err)
		return
	}
	dao.DB.Model(&order).Update(dao.AllOrders{
		ResHrmuserID: jwtID.ID,
		State:        atoi,
	})
	c.JSON(http.StatusOK, "状态变更成功")
}
func ResHrmUserIDByself(c *gin.Context) {
	atoi, e := strconv.Atoi(c.Param("state"))
	if e != nil {
		c.JSON(http.StatusOK, e.Error())
		return
	}
	fields := strings.Fields(c.Request.Header.Get("authorization")) //测试使用暂时这样
	jwtID, _ := GetJwtID(fields[1])
	var order []dao.AllOrders

	dao.DB.Where(&dao.AllOrders{
		ResHrmuserID: jwtID.ID,
		State:        atoi,
	}).Find(&order)

	c.JSON(http.StatusOK, &order)

}