package server

import (
	"github.com/gin-gonic/gin"
	"godear/dao"
	"log"
	"net/http"
)

func CreateHrm(c *gin.Context) {
	var hrm dao.Hrmusers
	err := c.BindJSON(&hrm)
	if err != nil {
		c.JSON(http.StatusOK, err.Error())
		return
	}
	dao.DB.Create(&hrm)
	c.JSON(http.StatusOK, hrm)
}

func DeleteHrmByID(c *gin.Context) {
	param := c.Param("id")
	var hrm dao.Hrmusers
	first := dao.DB.First(&hrm, param).Error
	if first != nil {
		log.Println(first.Error())
		c.JSON(http.StatusOK, first.Error())
		return
	}
	dao.DB.Delete(&hrm)
	c.JSON(http.StatusOK, "delele it success")
}

func UpdateHrmByID(c *gin.Context) {
	//param := c.Param("id")
	//var hrm dao.Hrmusers

}