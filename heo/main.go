package main

import (
	"heo/dao"
	"heo/serve/qrcode"
	"heo/servers"
	"log"
	"os"
	"time"
)

func main() {
	t := time.Now().Format("20060102")
	logname := "./log/" + t + ".log"
	logFile, _ := os.OpenFile(logname, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0777)
	log.SetOutput(logFile)
	dao.GO()
	qrcode.Qrcode("这个二维码2息2请查看", "我的二维码.png")

	routers := servers.Routers()
	routers.Run(":9000")

}

//hashfilemd5, err := hash.Hashfilemd5("./西部世界第三季第01集中英双字.mkv")
//if err != nil {
//log.Println(err)
//}
//fmt.Println(hashfilemd5)
