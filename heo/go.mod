module heo

go 1.13

require (
	github.com/boombuler/barcode v1.0.0
	github.com/gin-gonic/gin v1.6.2
	github.com/jinzhu/gorm v1.9.12
	github.com/julienschmidt/httprouter v1.3.0
)
