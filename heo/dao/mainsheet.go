package dao

type Hrms struct {
	ID         int    `json:"id"`
	Name       string `json:"name"`
	Phone      string `json:"phone"`
	Adress     string `json:"adress"`
	Coin       int    `json:"coin"`
	Prcturesrc string `json:"prcturesrc"`
}
type Orders struct {
	ID         int    `json:"id"`
	HrmID      int    `json:"hrm_id"`
	Createtime string `json:"createtime"`
	State      int    `json:"state"`
	Excoin     int    `json:"excoin"`
	Number     int    `json:"number"`
	Orderid    string `json:"orderid"`
	Content    string `json:"content"`
	Remark     string `json:"remark"`
	RecehrmID  int    `json:"recehrm_id"`
	Recetime   string `json:"recetime"`
}
