package dao

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"log"
)

var DB *gorm.DB
var dataname = "sqlite3"
var SqlDir = "./dataBase.db"

func GO() {
	log.Println("系统启动中")
	db, err := gorm.Open(dataname, SqlDir)
	if err != nil {
		log.Println(err)

	}
	db.AutoMigrate(&Hrms{}, &Orders{})
	DB = db
}

type Content interface {
	CreateHrmUser()
	CreateOrder()
	UpdataOrder()
}


func (o *Orders) UpdataOrder(id int) {
	DB.Model(&Orders{}).Where("id =?", id).Update(o)
}
func (h *Hrms) UpdataHrm(id int) {
	var hrm Hrms
	DB.Model(&hrm).Where("id =?", id).Update(h)
}
