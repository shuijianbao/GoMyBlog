package dao

import "log"

//创建人力
func (h *Hrms) CreateHrmUser() (err error) { //注册人员
	errs := DB.Create(h).Error
	if err != nil {
		log.Println(err)
		return errs
	}
	return nil
}

//修改个人信息
func (h *Hrms) UpdataHrmUser(id int) (err error) {
	hrm := &Hrms{}
	n := DB.First(hrm, id).Error
	if n != nil {
		log.Println(n)
		return n

	}
	e := DB.Model(hrm).Where("id = ?", id).Update(&h).Error
	if err != nil {
		log.Println(e)
		return e
	}
	return nil
}

//创建订单
func (o *Orders) CreateOrder() (err error) { //创建一个新的订单
	errs := DB.Create(o).Error
	if errs != nil {
		log.Println(errs)
		return errs
	}
	return nil
}

//通过ID定位并修改数据
func (or *Orders) UpdataOrderByID(id int) (e error) {
	order := Orders{}
	n := DB.First(&order, id).Error
	if n != nil {
		log.Println(n)
		return n
	}
	err := DB.Model(&order).Where("id = ?", id).Update(or).Error
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}
