package hash

import (
	"crypto/md5"
	"encoding/hex"
	"io"
	"log"
	"os"
)

func Hashfilemd5(filepath string) (md5str string, err error) {
	file,err:=os.Open(filepath)
	if err!=nil{
		log.Println(err)
		return
	}
	defer file.Close()
	hash:= md5.New()
	if _,err= io.Copy(hash,file);err!=nil{
		return
	}
	hashInbytes:= hash.Sum(nil)[:16]
	md5str = hex.EncodeToString(hashInbytes)
	return
}
