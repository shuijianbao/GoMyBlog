package qrcode

import (
	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/qr"
	"image/png"
	"os"
)

func Qrcode(data string, name string) {
	qrCode, _ := qr.Encode(data, qr.M, qr.Auto)
	qrCode, _ = barcode.Scale(qrCode, 256, 256)
	file, _ := os.Create(name)
	defer file.Close()
	png.Encode(file, qrCode)
}
