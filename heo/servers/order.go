package servers

import (
	"github.com/gin-gonic/gin"
	"heo/dao"
	"log"
	"net/http"
)
const (
	RES = "请求失败"
)
//结构体
type REA struct {
	Data interface{} `json:"data"`
}

//错误返回
type errs struct {
	data interface{} `json:"data"`
}

func createorder(c *gin.Context) {

	var or dao.Orders
	c.BindJSON(&or)
	if err := or.CreateOrder(); err != nil {
		log.Println(err)
		return
	}
	c.JSON(http.StatusOK, or)
}

//通过id变更订单

func updataorderbyid(c *gin.Context) {

	or := &dao.Orders{}
	c.BindJSON(or)
	err := or.UpdataOrderByID(or.ID)
	r := &REA{
		Data: err.Error(),
	}
	if err != nil {
		c.JSON(http.StatusOK, r)
		return
	}
	c.JSON(http.StatusOK, or)
}

//注册hrm
func createhrm(c *gin.Context) {
	hrm := &dao.Hrms{}
	c.BindJSON(hrm)
	err := hrm.CreateHrmUser()
	if err != nil {
		c.JSON(http.StatusOK, err.Error())
		return
	}
	c.JSON(http.StatusOK, hrm)

}

//修改hrm信息
func updatahrm(c *gin.Context) {
	hrm := &dao.Hrms{}
	c.BindJSON(hrm)
	err := hrm.UpdataHrmUser(hrm.ID)
	if err != nil {
		c.JSON(http.StatusOK, err.Error())
		return
	}
	c.JSON(http.StatusOK, hrm)
}
