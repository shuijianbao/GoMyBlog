package servers

import (
	"github.com/gin-gonic/gin"
)

func Routers() *gin.Engine {
	router := gin.Default()

	router.POST("/createhrm", createhrm)
	router.POST("/updatahrm", updatahrm)

	router.POST("/createorder", createorder)
	router.POST("/updataorderbyid", updataorderbyid)
	return router

}
