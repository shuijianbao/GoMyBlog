module upload

go 1.12

require (
	github.com/gin-gonic/gin v1.5.0
	github.com/go-macaron/binding v1.0.1 // indirect
	github.com/go-macaron/cache v0.0.0-20191101040916-6575fa0eaf7b // indirect
	github.com/go-macaron/session v0.0.0-20191101041208-c5d57a35f512 // indirect
)
