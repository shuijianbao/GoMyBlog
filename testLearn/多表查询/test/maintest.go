package main

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type User struct {
	ID        int
	Uname     string
	Languages []Language `gorm:"many2many:user_languages";"ForeignKey:UserId"`
}

type Language struct {
	ID   int
	Name string
}

type UserLanguages struct {
	UserId     int
	LanguageId int
}

func main() {
	db, err := gorm.Open("sqlite3", "./mydb.db")
	defer db.Close()
	if err != nil {
		panic(err)
	}
	//db.LogMode(true)

	defer db.Close()
	//db.DropTableIfExists(&Contact{},&Customer{})

	//生成数据库表
	db.AutoMigrate(&User{}, &Language{}, &UserLanguages{})

	//db.Model(UserLanguages{}).AddForeignKey("user_id","user(id)","CASCADE","CASCADE")
	//db.Model(UserLanguages{}).AddForeignKey("language_id","language(id)","CASCADE","CASCADE")

	langs := []Language{{Name:"English"},{Name:"Janpanies"},{Name:"Franch"}}

	user1 := User{Uname:"ChenY",Languages:langs}
	user2 := User {Uname:"LUS",Languages:langs}

	// 级联添加
	db.Save(&user1)
	db.Save(&user2)

	fmt.Println(&user1)
	fmt.Println(&user2)

	// 级联查询
	var user User
	db.Where("id = ?",2).Preload("Languages").Find(&user)
	fmt.Println(user)

	//级联删除 未实现
	// 仅能删除主表数据。求大神指点。

}

//链接：https://blog.csdn.net/sss996/article/details/94047101