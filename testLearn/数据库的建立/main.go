package main

import (
	"bufio"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"io"
	"io/ioutil"
	"log"
	"os"
)

//数据库结构体的建立
//所有的文件名称
type AllFileName struct {
	gorm.Model
	FileName string
}

//文件夹名
type FlodName struct {
	gorm.Model
	FlodsName string
	FileTier  int64
	FlodPath  string
	Personnel string
}

//人力资源表
type Personnel struct {
	gorm.Model
	Name string
}

//层级关系
type FileTier struct {
	Tier uint
}

var db *gorm.DB
var err error

func main() {
	folename("./",0)
	//数据库结构体的建立
	db, err := gorm.Open("sqlite3", "./mydb.db")
	if err != nil {
		log.Panic(err)
	}
	defer db.Close()
	db.AutoMigrate(
		&AllFileName{},
		&FlodName{},
		&Personnel{},
		&FileTier{},
	)

	r := gin.Default()

	r.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"name": "ok",
		})
	})

	for _,i:=range fo{
		db.Create(&i)
	}
	r.POST("/fileapi/:personalid/:tier", UploadAndFileName)
	r.Run(":8000")
}

type UploadPaths struct {
	Personnel string
	Tier      string
}

//接收单个文件并创建文件夹，并写入相关的层级关系
//层级关系的定义，应该从接口直接定义了
func UploadAndFileName(c *gin.Context) {

	file, header, err := c.Request.FormFile("file")
	defer file.Close()
	//文件上传格式：http://127.0.0.1:port/:fileapi/:personalid/:tier
	//接收文件路径
	var uploadpaths UploadPaths
	uploadpaths.Personnel = c.Param("personalid")
	uploadpaths.Tier = c.Param("tier")

	//根据接口创建文件目录
	uploadDir := uploadpaths.Personnel + "/"
	os.MkdirAll(uploadDir, os.ModePerm)
	createfile, err := os.Create(uploadDir + header.Filename)
	if err != nil {
		log.Println(err)
	}
	//然后将文件写如相关文件夹内
	if err != nil {
		log.Println(err)
	}
	buf := make([]byte, 1024)
	//创建文件路径
	reader := bufio.NewReader(file)
	for {
		_, err := reader.Read(buf)
		if err == io.EOF {
			break
		}
		createfile.Write(buf)
	}
	defer createfile.Close()
	//将文件的信息写入数据库 包括文件名 还有大小 所属人员
	var alienate AllFileName
	fmt.Println("-------------------------")

	alienate.FileName = "header.Filename"
	db.Create(&alienate)


	c.JSON(200, "ok")
}

/***func teat() {
var allfilename AllFileName
	var flodname FlodName

	allfilename.FileName = header.Filename
	allfilename.FilePath = uploadDir
	flodname.Personnel = uploadpaths.Personnel
	db.Create(&allfilename)
	db.Create(&flodname)
	fmt.Println("okk-----------------------------------------")
}***/
var fo []FlodName
func folename (dir string,num int64){
	infos, err := ioutil.ReadDir(dir)
	if err!=nil{
		log.Println(err)
	}

	for _,fi:=range infos{
		fmt.Println(fi.Name(),num)
		fo= append(fo,FlodName{FlodsName:fi.Name(),FileTier:num,FlodPath:dir+fi.Name()})
		if fi.IsDir(){
			folename(dir+fi.Name(),num+1)
		}
	}
}