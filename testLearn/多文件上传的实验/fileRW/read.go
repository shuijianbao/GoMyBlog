package fileRW

import (
	"bufio"
	"io"
	"mime/multipart"
)

func Readfile(r multipart.File) {
	buf := make([]byte, 2096)
	reader := bufio.NewReader(r)
	for {
		_, err := reader.Read(buf)
		if err == io.EOF {
			break
		}
	}

}
