package main

import (
	"bufio"
	"fmt"
	"github.com/gin-gonic/gin"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"reflect"
)



//上传对个文件
func up(c *gin.Context) {
	c.Request.ParseMultipartForm(32 << 20)
	multipartForm := c.Request.MultipartForm.File["file"]
	fmt.Println(reflect.TypeOf(multipartForm))
	len := len(multipartForm)
	for i := 0; i < len; i++ {
		fmt.Println(multipartForm[i])
	}
}
func upmore(c *gin.Context) {
	c.Request.ParseMultipartForm(32 << 20)
	headers := c.Request.MultipartForm.File["file"]
	lens := len(headers)
	for i := 0; i < lens; i++ {
		file, err := headers[i].Open()
		if err != nil {
			fmt.Println(err)
			return
		}
		defer file.Close()
		os.Mkdir("./up/", os.ModePerm)
		createe, err := os.Create("./up/" + headers[i].Filename)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println("doing")
		defer createe.Close()
		io.Copy(createe, file)
	}
	c.String(200, "nihao")
}
func upone(c *gin.Context) {
	file, header, _ := c.Request.FormFile("file")
	fmt.Println(header.Filename)
	os.Mkdir("./upload", os.ModePerm)
	cur, _ := os.Create("./upload/" + header.Filename)
	defer cur.Close()
	buf := make([]byte, 1024)
	reader := bufio.NewReader(file)
	for {
		_, err := reader.Read(buf)
		if err == io.EOF {
			break
		}
		cur.Write(buf)
	}
	defer file.Close()
	c.String(200, "ollkkk")
}
func uploadOne(w http.ResponseWriter, r *http.Request) {
	//判断请求方式
	if r.Method == "POST" {
		//设置内存大小
		r.ParseMultipartForm(32 << 20);
		//获取上传的第一个文件
		file, header, err := r.FormFile("file");
		defer file.Close();
		if err != nil {
			log.Fatal(err);
		}
		//创建上传目录
		os.Mkdir("./upload", os.ModePerm);
		//创建上传文件
		cur, err := os.Create("./upload/" + header.Filename);
		defer cur.Close();
		if err != nil {
			log.Fatal(err);
		}
		//把上传文件数据拷贝到我们新建的文件
		io.Copy(cur, file);
	} else {
		//解析模板文件
		t, _ := template.ParseFiles("./uploadOne.html");
		//输出文件数据
		t.Execute(w, nil);
	}
}

func uploadMore(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		//设置内存大小
		r.ParseMultipartForm(32 << 20);
		//获取上传的文件组
		files := r.MultipartForm.File["file"];
		len := len(files);
		for i := 0; i < len; i++ {
			//打开上传文件
			file, err := files[i].Open();
			defer file.Close();
			if err != nil {
				log.Fatal(err);
			}
			//创建上传目录
			os.Mkdir("./upload", os.ModePerm);
			//创建上传文件
			cur, err := os.Create("./upload/" + files[i].Filename);
			defer cur.Close();
			if err != nil {
				log.Fatal(err);
			}
			io.Copy(cur, file);
		}
	} else {
		//解析模板文件
		t, _ := template.ParseFiles("./uploadMore.html");
		//输出文件数据
		t.Execute(w, nil);
	}
}

func main() {
	r := gin.Default()
	r.Use(cors())

	r.POST("/upmore", upmore)
	r.Run(":6001")

}
func cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Headers", "Content-Type,AccessToken,X-CSRF-Token, Authorization, Token")
		c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
		c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type")
		c.Header("Access-Control-Allow-Credentials", "true")
		if method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
		}
		c.Next()
	}
}
