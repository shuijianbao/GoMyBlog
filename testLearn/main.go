package main

import (
	"bufio"
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
	"net/http"
	"os"
	"reflect"
)

func main() {
	r := gin.Default()
	r.POST("/upload", upfile)
	r.Run(":6001")
}

func upfile(c *gin.Context) {
	file, header, err := c.Request.FormFile("upload")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(reflect.TypeOf(file))

	savefile, err := os.OpenFile(header.Filename, os.O_RDWR, os.ModePerm)
	if err != nil {
		savefile, _ = os.Create(header.Filename)
	}
	buf := make([]byte, 4096)
	fmt.Println(reflect.TypeOf(buf))
	reader := bufio.NewReader(file)
	for {
		_, err := reader.Read(buf)
		if err == io.EOF {
			break
		}
		savefile.Write(buf)
	}

	savefile.Close()
	c.String(http.StatusOK, "scuess")
}
